#! /usr/bin/env bash
set -euoE pipefail

ES_NAME=alexos
ES_HOST='localhost:9200'
NAMESPACE=elastic

if [[ -z ${GCP_PROJECT_ID:-} ]]; then echo "GCP Project not set"; exit 1; fi
if [[ -z ${MY_EMAIL:-} ]]; then echo "Email not set"; exit 1; fi

es_password=$(kubectl get secret alexos-es-elastic-user -n ${NAMESPACE} -o json | jq -r '.data.elastic' | base64 -d)
logstash_writer_password=$(gcloud secrets versions access latest --project="${GCP_PROJECT_ID}" --secret=logstash-password)
my_kibana_password=$(gcloud secrets versions access latest --project="${GCP_PROJECT_ID}" --secret=my-kibana-login)


function main() {
  apt-get update && apt-get install -y procps

  # wait for ES to be healthy
  while : ; do
    phase=$(kubectl get ElasticSearch ${ES_NAME} -n ${NAMESPACE} -o 'jsonpath={..status.phase}')
    if [[ "$phase" = "Ready" ]]; then
        break
    fi
    echo "Waiting for the ElasticSearch Phase to become Ready, currently it is ${phase}" && sleep 15
  done

  kubectl port-forward -n ${NAMESPACE} svc/${ES_NAME}-es-http 9200 &

  wait-for-url "http://${ES_HOST}/_nodes"

  # ensure "secure settings" reloaded - i.e. secrets exists/are loaded
  echo "-> Reloading Secure Settings on all nodes"
  curl -X POST -u "elastic:$es_password" "http://${ES_HOST}/_nodes/reload_secure_settings"

  # Create "logstash_writer" role
  logstash_writer_role='{
        "cluster": ["manage_index_templates", "monitor", "manage_ilm", "manage_ingest_pipelines"],
        "indices": [{ "names": [ "logstash*" ], "privileges": ["all"] }]
    }'
  curl_es 'Creating "logstash_writer" role' '_security/role/logstash_writer' "${logstash_writer_role}"

   # Create "logstash_writer" user
  logstash_writer_user='{
        "password": "'${logstash_writer_password}'",
        "roles": ["logstash_writer"],
        "full_name": "Logstash Writer"
    }'
  curl_es 'Creating "logstash_writer" user' '_security/user/logstash_writer' "${logstash_writer_user}"

  # Create "logstash_reader" role
  logstash_reader_role='{
      "cluster": [],
      "indices": [{ "names": [ "logstash-*" ], "privileges": ["read"] }]
  }'
  curl_es 'Creating "logstash_reader" role' '_security/role/logstash_reader' "${logstash_reader_role}"

  # create my user
  my_user='{
        "password": "'${my_kibana_password}'",
        "roles": ["superuser"],
        "full_name": "Alex M",
        "email": "'${MY_EMAIL}'"
    }'
  curl_es 'Creating my kibana user' '_security/user/alex' "${my_user}"

  # ILM policies
  ilm_dlq=$(cat elastic/es-ilm-policies/ilm-policy-dlq.json)

  curl_es 'Creating "logstash-dlq-k8s" ILM policy' '_ilm/policy/logstash-dlq-k8s' "${ilm_dlq}"

  pkill kubectl

}

function curl_es() {
  message=${1}
  endpoint=${2}
  json=${3}

  echo "-> ${message}"

  curl \
    --fail \
    -X PUT \
    -H 'Content-Type: application/json' \
    -d "${json}" \
    -v \
    "http://elastic:${es_password}@${ES_HOST}/${endpoint}"
}

function wait-for-url() {
    echo "Testing $1"
    timeout -s TERM 60 bash -c \
    'while [[ "$(curl -s -o /dev/null -L -w ''%{http_code}'' -u "elastic:'$es_password'" ${0})" != "200" ]];\
    do echo "Waiting for ${0}" && sleep 5;\
    done' "${1}"
    echo "OK!"
}

main "$@"
