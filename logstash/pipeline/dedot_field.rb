def register(params)
	@source_fields = params["source_fields"]
end

def collapse_nested_keys(dedotted_object, input_object, parent_key)
    input_object.each do |key, value|
        @current_full_key = parent_key + (parent_key != "" ? "_" : "") + key
        if value.is_a?(Hash)
            collapse_nested_keys(dedotted_object, value, @current_full_key)
        else
           dedotted_object[@current_full_key] = value
        end
    end
end

def filter(event)
    begin
      @source_fields.each { |source_field|
        next if source_field.nil?
        object = event.get(source_field)
        unless object.nil?
            @dedotted_object = {}
            collapse_nested_keys(@dedotted_object, object, '')
            event.set(source_field, @dedotted_object)
        end
      }
    rescue Exception => e
         event['ruby_exception'] = 'dedot_field.rb failed: ' + e.message
    end
    return [event]
end

test "document with labels with dots" do
    parameters do
    { "source_fields" => [
            "[protoPayload][request][spec][template][metadata][labels]",
             "[protoPayload][response][spec][selector][matchLabels]"
        ]
    }
    end

    in_event { {
               "protoPayload" => {
                   "request" => {
                     "spec" => {
                       "template" => {
                         "metadata" => {
                           "labels" => {
                               "app" => {
                                    "kubernetes" => {
                                       "io/component" => "foo",
                                       "io/name" => "bar",
                                       "io/instance" => "hello"
                                    }

                               },
                               "simple"=> "string"
                           }
                         }
                       }
                     }
                   },
                    "response" => {
                        "spec" => {
                          "selector" => {
                              "matchLabels" => {
                                  "app" => {
                                       "kubernetes" => {
                                          "io/component" => "foo",
                                          "io/name" => "bar",
                                          "io/instance" => "hello"
                                       }

                                  },
                                  "simple"=> "string"
                              }
                          }
                        }
                     }
               }
    } }

    expect("no ruby_exception logged") {|events| events.first.get("[ruby_exception]") == nil  }
    expect("labels: app_kubernetes_io/component is collapsed") {|events| events.first.get("[protoPayload][request][spec][template][metadata][labels][app_kubernetes_io/component]") == "foo" }
    expect("labels: app_kubernetes_io/name is collapsed") {|events| events.first.get("[protoPayload][request][spec][template][metadata][labels][app_kubernetes_io/name]") == "bar" }
    expect("labels: app_kubernetes_io/instance is collapsed") {|events| events.first.get("[protoPayload][request][spec][template][metadata][labels][app_kubernetes_io/instance]") == "hello" }
    expect("labels: simple string has been preserved") {|events| events.first.get("[protoPayload][request][spec][template][metadata][labels][simple]") == "string" }
    expect("labels: nested app object no longer exists") {|events| events.first.get("[protoPayload][request][spec][template][metadata][labels][app]") == nil  }

    expect("matchLabels: app_kubernetes_io/component is collapsed") {|events| events.first.get("[protoPayload][response][spec][selector][matchLabels][app_kubernetes_io/component]") == "foo" }
    expect("matchLabels: app_kubernetes_io/name is collapsed") {|events| events.first.get("[protoPayload][response][spec][selector][matchLabels][app_kubernetes_io/name]") == "bar" }
    expect("matchLabels: app_kubernetes_io/instance is collapsed") {|events| events.first.get("[protoPayload][response][spec][selector][matchLabels][app_kubernetes_io/instance]") == "hello" }
    expect("matchLabels: simple string has been preserved") {|events| events.first.get("[protoPayload][response][spec][selector][matchLabels][simple]") == "string" }
    expect("matchLabels: nested app object no longer exists") {|events| events.first.get("[protoPayload][response][spec][selector][matchLabels][app]") == nil  }
end

test "document without labels" do
    parameters do
    { "source_fields" => [
            "[protoPayload][request][spec][template][metadata][labels]",
             "[protoPayload][response][spec][selector][matchLabels]"
        ]
    }
    end

    in_event { { "protoPayload" => { } } }
    expect("documented is preserved") {|events| events.first.get("[protoPayload]") != nil }
end
