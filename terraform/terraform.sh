#!/usr/bin/env bash
set -euoE pipefail

if [[ -z ${GCP_PROJECT_ID:-} ]]; then echo "Missing GCP Project"; exit 1; fi
if [[ -z ${CLUSTER_NAME:-} ]]; then echo "Missing Cluster Name"; exit 1; fi
if [[ -z ${REGION:-} ]]; then echo "Missing Cluster Region"; exit 1; fi

echo "-> Initialising terraform"
terraform init -backend-config=bucket=e-rampart-315307-apps-tfstate -backend-config=prefix=elastic

echo "-> Applying terraform"
terraform apply -auto-approve -var gcp_project_id="${GCP_PROJECT_ID}" -var cluster_name="${CLUSTER_NAME}" -var region="${REGION}"
