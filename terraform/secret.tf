resource "google_secret_manager_secret" "logstash-password" {
  secret_id = "logstash-password"
  project = var.gcp_project_id
  labels = {
    app = "logstash"
  }
  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret" "my-kibana-login" {
  secret_id = "my-kibana-login"
  project = var.gcp_project_id
  labels = {
    app = "kibana"
  }
  replication {
    automatic = true
  }
}

resource "google_secret_manager_secret_iam_member" "logstash-password" {
  project   = var.gcp_project_id
  secret_id = google_secret_manager_secret.logstash-password.id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:gke-deployer@${var.gcp_project_id}.iam.gserviceaccount.com"
}

resource "google_secret_manager_secret_iam_member" "my-kibana-login" {
  project   = var.gcp_project_id
  secret_id = google_secret_manager_secret.my-kibana-login.id
  role      = "roles/secretmanager.secretAccessor"
  member    = "serviceAccount:gke-deployer@${var.gcp_project_id}.iam.gserviceaccount.com"
}

resource "random_string" "logstash_writer_password" {
  length  = 21
  special = false
}

resource "kubernetes_secret" "logstash" {
  metadata {
    name      = "logstash"
    namespace = "elastic"
    labels = {
      app = "logstash"
    }
  }

  data = {
    LOGSTASH_WRITER_PASSWORD     = random_string.logstash_writer_password.result
  }
}