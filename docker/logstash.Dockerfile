FROM docker.elastic.co/logstash/logstash:7.13.2

USER root
RUN yum -y update
USER logstash

RUN /usr/share/logstash/bin/logstash-plugin install logstash-filter-range
RUN /usr/share/logstash/bin/logstash-plugin install logstash-filter-de_dot
