# alexos-logging

EFK stack deployed to my cluster using the ECK Operator

---

## elastic-operator

This repo deploys elastic operator in elastic-system namespace along with the CRDs.

The file `eck-1.5.0-all-in-one.yaml` is an unmodified copy of [ECK all-in-one](https://download.elastic.co/downloads/eck/1.5.0/all-in-one.yaml) as directed in [the docs](https://www.elastic.co/guide/en/cloud-on-k8s/current/k8s-deploy-eck.html). This file should be wholly replaceable when upgrading.

The other files are patches.

## Elastic & Kibana

We then use the ECK operator to create Elastic + Kibana instances. Things are kept as simple as possible for this basic cluster - so no separate masters or hot/warm architecture (we are pretty constrainted on space).

## Users & Passwords

These are safely stored in Google Cloud SecretManager and not available in source code.

They are set with:

```sh
 echo -n {$value} | gcloud secrets versions add ${SECRET_NAME} --project=${GCP_PROJECT_ID} --data-file=-
```

And retrieved with:

```sh
gcloud secrets versions access latest --project=${GCP_PROJECT_ID} --secret=${SECRET_NAME}
```

---

## To Do

- [x] install operator
- [x] install elasticsearch
- [x] install kibana
- [ ] auth for kibana
- [x] install filebeat
- [x] install logstash
- [ ] fix for logstash password generated manually: `k create secret generic logstash --from-literal=LOGSTASH_WRITER_PASSWORD=(value)`
- [ ] get container logs in
- [ ] get k8s events in
- [ ] get cloudaudit logs in
- [ ] get gclb logs in
- [ ] set up ILM to protect disk
- [ ] set up DLQ
- [ ] set up snapshot/restore functionality
- [ ] prometheus monitoring of the stack
- [ ] refine PSPs - very permissive currently
